<?php
namespace App;

use mikehaertl\wkhtmlto\Pdf;

/**
 * Class PdfConverter
 * @package App
 */
class PdfConverter
{

    public function perform(string $rawHtml): string
    {
        $html = $this->base64_url_decode($rawHtml);
        $layout = file_get_contents(__DIR__ . '/views/layout.html');
        $content = str_replace('{{CONTENT}}', $html, $layout);

        $pdf = new Pdf([
            'no-outline',
            'dpi' => 220,
            'margin-top' => 0,
            'margin-right' => 0,
            'margin-bottom' => 0,
            'margin-left' => 0,
            'disable-smart-shrinking',
        ]);
        $pdf->addPage($content);

        return $this->base64_url_encode($pdf->toString());
    }

    public function dump(string $rawHtml): string
    {
        $layout = file_get_contents(__DIR__ . '/views/layout.html');
        $content = str_replace('{{CONTENT}}', $rawHtml, $layout);

        $pdf = new Pdf([
            'no-outline',
            'dpi' => 220,
            'margin-top' => 0,
            'margin-right' => 0,
            'margin-bottom' => 0,
            'margin-left' => 0,
            'disable-smart-shrinking',
        ]);
        $pdf->addPage($content);

        $filename = 'pdf-' . time() . '.pdf';
        $path = __DIR__ . "/../web/pdf-files/{$filename}";

        file_put_contents($path, $pdf->toString());

        return $path;

    }

    function base64_url_encode($input) {
        return strtr(base64_encode($input), '+/=', '-_,');
    }

    function base64_url_decode($input) {
        return base64_decode(strtr($input, '-_,', '+/='));
    }
}