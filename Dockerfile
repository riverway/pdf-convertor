FROM riverway/php7:latest

MAINTAINER Victor <victor@riverway.it>

RUN apt-get update
RUN apt-get install -y git-core zip wkhtmltopdf xvfb wget zlib1g-dev
RUN mv /usr/bin/wkhtmltopdf /usr/bin/wkhtmltopdf-origin
COPY docker/pdf-convertor/php.ini /usr/local/etc/php/php.ini
COPY docker/pdf-convertor/wkhtmltopdf /usr/local/bin
RUN chmod +x /usr/local/bin/wkhtmltopdf


COPY docker/pdf-convertor/start.sh /usr/local/bin/docker-app-start

RUN chmod +x /usr/local/bin/docker-app-start
RUN mkdir -p /srv/app
WORKDIR /srv/app

CMD ["docker-app-start"]

