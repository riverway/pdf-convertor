<?php
require_once __DIR__ . '/../vendor/autoload.php';

(new Symfony\Component\Dotenv\Dotenv())->load(__DIR__ . '/../app.env');

$app = new Silex\Application();
$hosts = explode(':',getenv('TRUSTED_HOSTS'));
\Symfony\Component\HttpFoundation\Request::setTrustedHosts($hosts);

$app->register(new Silex\Provider\MonologServiceProvider(), array(
    'monolog.logfile' => __DIR__ . '/../var/logs/app.log',
));
$app->extend('monolog', function($monolog, $app) {
    if(getenv('SENTRY_URL')) {
        $client = new Raven_Client(getenv('SENTRY_URL'));
        $handler = new Monolog\Handler\RavenHandler($client);
        $monolog->pushHandler($handler);
    }
    return $monolog;
});
$app->post('/convert', function (\Symfony\Component\HttpFoundation\Request $request) use ($app) {
    $html = $request->request->get('encoded_html');

    $converter = new \App\PdfConverter();
    return new \Symfony\Component\HttpFoundation\Response($converter->perform($html));
});

$app->post('/dump', function (\Symfony\Component\HttpFoundation\Request $request) use ($app) {
    $html = $request->request->get('encoded_html');
    $converter = new \App\PdfConverter();

    $path = $converter->dump($html);
    $response = new \Symfony\Component\HttpFoundation\BinaryFileResponse($path);

    $response->headers->set('Content-Type', 'application/pdf');
    $response->setContentDisposition(
        \Symfony\Component\HttpFoundation\ResponseHeaderBag::DISPOSITION_INLINE
    );

    return $response;
});

$app->get('/', function () use ($app) {
    return 'Hi!';
});
$app->run();