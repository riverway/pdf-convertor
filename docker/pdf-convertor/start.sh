#!/bin/sh
set -xe

composer install --prefer-dist --no-suggest && composer clear-cache
chown -R www-data var
exec php-fpm